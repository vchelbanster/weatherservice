# weather
Assignment project


# Description
Minimalistic and intuitive app, handy for getting insights about the weather around you.
The app takes advantage of more recent Android features for executing work in background at scheduled intervals - JobScheduler API
Even though the solution it's not by the letter following MVP architecture, an attempt was made through the existing android components (Activity/Fragment/AsyncLoader-JobService)

App requires Android API 21+ and Google Play Services installed to work properly.

`Note:` the requirement for OpenWeatherMap API is to make an HTTP request in 3 hours (even though the assignment description says 2 hours). I set it to 3 hours to conform with the Rest API, but it's configurable in

# App Design


### Data Storage

Save json data file to persistence - the data payload is relatively small and doesn't have relational data; SharedPrefs drops from the start (complexity); using file persistence because the data is used only for one purpose (having a Widget will change that)

### Data Retrieval & processing

- Downloading using retrofit - simple and easy to configure
- Processing using GSON - fast and easy Json mapping; and the memory footprint it's not large enough to wory about
- Retrieving/processing in a JobService, scheduled using JobSheduler APIs - the go-go API for background work starting API 21 (configurable, resilient)

### Data presentation

- Main container: fragment - separate functional from presentation (fragment being the data presenter)
- RecyclerView for displaying list data - the name says it already (recycles the list items with a framework in place). Pretty flexible for lots of stuff (like grouping forecast items by day :))

### Location

Considering Weather API requirements for location(city-level location), I decided to go with Google Play Services fused location provider, with low-power mode.
It's accurate enough for city-level location and it's not draining the battery with long GPS location requests.
Considering most of the users and the above requirements, this approach is wise and balanced.

### Stuff to improve/Ideas for the future

- Make unit-test passing: avoid built-in immutability for classes if possible
- Multiple locations support (user manageable)
- Improved UX
   - Improving the UI feedback while loading data (show progress while loading)
   - Change items from forecast list to CardView, containing more information
   - Adjust color scheme / theme
   - Hide FAB while scrolling down (show only when on top of the list)
   - Open list item details in a separate fragment
- Created a widget for user's convenience
- Improve Kotlin code readability/aesthetic look: use "syntactic sugar"
- Improve on asynchronous execution: either use co-routines or RxAndroid/RxJava 
- Make use of dagger-android library: use bindings for injecting dependencies in activities/fragments/services
- Handle gracefully background data limitation (if any) and other non-favorable scenarios (yup - didn't handle them)
- Add share functionality for an individual forecast or a group
- More Unit testing and functional testing


### How to build
Considering that:
 - all the source code is checkout out locally
 - Android Studio 3.1.4 is installed and/or build tools 27.0.3 and platform v27 is downloaded locally

The application can be built using either one of the following approaches:
 - Use command-line:
    - Navigate to the project top-level directory using any terminal/console
    - Run the following command: `./gradlew clean assembleDebug` (unix-based systems - windows is similar)
    
 - Use Android Studio IDE:
    - Using AS import the project from the checked-out location and allow to finish all the synchronization for gradle
    - From `Build` top-level menu entry select `Build APK(s)` or
    - From the IDE run menu select to `Run` the "app" module
    
In order to install the previous-built application to a device/emulator the `adb` executable from Android SDK `platform-tools` is required (this step is not covered here).
