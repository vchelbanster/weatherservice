package com.bazyle.weatherservice.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat

fun Context.isLocationPermissionGranted() = PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this,
        Manifest.permission.ACCESS_FINE_LOCATION)