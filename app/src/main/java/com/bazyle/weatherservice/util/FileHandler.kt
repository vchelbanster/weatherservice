package com.bazyle.weatherservice.util

import android.content.Context
import android.util.Log

import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException
import java.io.Reader
import java.io.Writer

import javax.inject.Inject

/**
 * File-specific I/O operations container class.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */

class FileHandler @Inject constructor(val appContext: Context) {

    /**
     * Get a handle to the json data file used for caching network-retrieved data.
     *
     *
     * If the file doesn't exist it's created automatically.
     *
     * @return The data file or `null` if some I/O failures occur.
     */
    val dataFile: File
        get() {
            val jsonFile = File(appContext.externalCacheDir, JSON_DATA_FILE)
            if (!jsonFile.exists()) {
                try {
                    jsonFile.createNewFile()
                } catch (e: IOException) {
                    Log.w(TAG, "Failed to create data file")
                }

            }
            return jsonFile
        }

    /**
     * Get a [Reader] for the cached data file.
     *
     * @return the newly created Reader for the data file.
     */
    val jsonDataReader: Reader?
        get() {
            try {
                return BufferedReader(FileReader(dataFile))
            } catch (e: IOException) {
                Log.w(TAG, "Failed to access/read data file")
            }

            return null
        }

    /**
     * Get a [Writer] for the cached data file.
     *
     * @return the newly created Writer for the data file.
     */
    val jsonDataWriter: Writer?
        get() {
            try {
                return BufferedWriter(FileWriter(dataFile))
            } catch (e: IOException) {
                Log.w(TAG, "Failed to access/write data file")
            }

            return null
        }

    companion object {
        private val JSON_DATA_FILE = "weather_forecast.json"
        private val TAG = FileHandler::class.java.simpleName
    }
}