package com.bazyle.weatherservice.util

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View
import com.bazyle.weatherservice.R

fun View.showMessage(@StringRes textRes: Int) = buildSnackbar(this, textRes, Snackbar.LENGTH_LONG).show()

fun View.showShortMessage(@StringRes textRes: Int) = buildSnackbar(this, textRes, Snackbar.LENGTH_SHORT).show()

fun View.showPermanentMessage(@StringRes textRes: Int) = buildSnackbar(this, textRes, Snackbar.LENGTH_INDEFINITE).show()

fun View.showMessage(@StringRes textRes: Int, action: () -> Unit) {
    buildSnackbar(this, textRes, Snackbar.LENGTH_LONG).setAction(R.string.action_positive) { action.invoke() }.show()
}

private fun buildSnackbar(anchor: View, @StringRes textRes: Int, duration: Int): Snackbar {
    return Snackbar.make(anchor, textRes, duration)
}