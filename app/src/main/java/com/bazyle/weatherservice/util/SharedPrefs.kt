package com.bazyle.weatherservice.util

import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import javax.inject.Inject


/**
 * Utility class for storing simple values using  [SharedPreferences].
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */

class SharedPrefs @Inject constructor(private var ctx: Context) {


    /**
     * Retrieve the last saved location latitude, determined by the app.
     *
     * @return location latitude
     */
    val locationLat: String
        get() = prefs.getString(KEY_LOCATION_LAT, "")

    /**
     * Retrieve the last saved location longitude, determined by the app.
     *
     * @return location longitude
     */
    val locationLon: String
        get() = prefs.getString(KEY_LOCATION_LON, "")

    private val prefs: SharedPreferences
        get() = ctx.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE)

    private val editor: SharedPreferences.Editor
        get() = prefs.edit()

    /**
     * Persist the location to shared preferences for later use.
     *
     *
     * A subsequent invocation to this method will replace the previous location with the new one.
     *
     * @param location updated to location to save.
     */
    fun saveLocation(location: Location) {
        editor.putString(KEY_LOCATION_LAT, location.latitude.toString())
                .putString(KEY_LOCATION_LON, location.longitude.toString())
                .apply()
    }

    companion object {
        private val PREFS_FILE_NAME = "prefs"
        private val KEY_LOCATION_STR = "short_location"
        private val KEY_LOCATION_LAT = "location_lat"
        private val KEY_LOCATION_LON = "location_lon"
    }


}
