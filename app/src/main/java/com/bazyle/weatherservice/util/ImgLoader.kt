package com.bazyle.weatherservice.util

import android.content.Context
import com.bazyle.weatherservice.ICON_URL_MASK

import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener

/**
 * Utility class for image-related functionalities.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
object ImgLoader {

    /**
     * Intializing UIL with sensible defaults.
     *
     * @param appContext
     */
    fun initImageLoader(appContext: Context) {
        val defaultOptions = DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .build()
        val config = ImageLoaderConfiguration.Builder(appContext)
                .defaultDisplayImageOptions(defaultOptions)
                .build()
        ImageLoader.getInstance().init(config)
    }

    /**
     * Load and display weather icon.
     *
     * @param iconName
     * @param imageAware
     * @param listener
     */
    fun displayWeatherIcon(iconName: String, imageAware: ImageViewAware, listener: ImageLoadingListener?) =
            ImageLoader.getInstance().displayImage(String.format(ICON_URL_MASK, iconName), imageAware, listener)
}
