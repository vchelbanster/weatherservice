package com.bazyle.weatherservice.ui


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.LoaderManager
import android.support.v4.content.Loader
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bazyle.weatherservice.App
import com.bazyle.weatherservice.R
import com.bazyle.weatherservice.model.ForecastResponse
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

/**
 * Fragment containing the forecast results
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
class ForecastListFragment : Fragment(), LoaderManager.LoaderCallbacks<ForecastResponse> {
    private var appName: String? = null

    @Inject
    lateinit var asyncLoader: JsonDataAsyncLoader
    private var listAdapter: ForecastListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupLoader()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitle(getString(R.string.app_name) + " - Loading...")
        forecast_list?.layoutManager = LinearLayoutManager(context)
        listAdapter = ForecastListAdapter()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        appName = getString(R.string.app_name)
        App.component.inject(this)
    }

    private fun setupLoader() {
        //initialize loader only if the location is defined...
        loaderManager.initLoader(0, null, this)
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<ForecastResponse> = asyncLoader

    override fun onLoadFinished(loader: Loader<ForecastResponse>, data: ForecastResponse?) = bindData(data)

    override fun onLoaderReset(loader: Loader<ForecastResponse>) {
        if (listAdapter != null) listAdapter?.reset()
    }

    private fun bindData(data: ForecastResponse?) {
        if (data?.list != null) {
            setTitle(appName + " - " + data.city!!.name)

            //hiding the guide - showing the list
            forecast_list?.visibility = View.VISIBLE
            empty_view?.visibility = View.GONE
            listAdapter?.setForecastItems(data.list)
            forecast_list?.adapter = listAdapter
        } else {
            //hiding the list - showing the guide
            setTitle(appName!! + " - Unknown")

            forecast_list?.visibility = View.GONE
            empty_view?.visibility = View.VISIBLE
        }
    }

    private fun setTitle(title: String) {
        activity?.title = title
    }

}