package com.bazyle.weatherservice.ui

import android.content.Context
import android.os.FileObserver
import android.support.v4.content.AsyncTaskLoader

import com.bazyle.weatherservice.model.ForecastResponse
import com.bazyle.weatherservice.util.FileHandler
import com.google.gson.Gson
import dagger.Lazy

import javax.inject.Inject

/**
 * [AsyncTaskLoader] for loading cached json data from persistence and creating mapped objects hierarchy.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */

class JsonDataAsyncLoader @Inject
constructor(context: Context) : AsyncTaskLoader<ForecastResponse>(context) {
    private var responseData: ForecastResponse? = null

    @Inject
    lateinit var fileHandler: FileHandler
    @Inject
    lateinit var gson: Lazy<Gson>

    private var fileObserver: FileObserver? = null


    override fun onStartLoading() {
        if (responseData != null) {
            //use data
            deliverResult(responseData)
        }

        if (fileObserver == null) {
            fileObserver = object : FileObserver(fileHandler.dataFile.path) {
                override fun onEvent(event: Int, path: String?) {
                    if (event == FileObserver.MODIFY || event == FileObserver.CLOSE_WRITE) {
                        //notify the loader to load
                        onContentChanged()
                    }
                }
            }
            fileObserver?.startWatching()
        }

        if (takeContentChanged() || responseData == null) {
            //smth changed or there's no data
            forceLoad()
        }
    }

    override fun loadInBackground(): ForecastResponse? {
        //creating only when needed
        return gson.get().fromJson(fileHandler.jsonDataReader, ForecastResponse::class.java)
    }

    override fun onReset() {
        fileObserver?.stopWatching()
        fileObserver = null
    }

    override fun deliverResult(data: ForecastResponse?) {
        responseData = data
        super.deliverResult(data)
    }

}