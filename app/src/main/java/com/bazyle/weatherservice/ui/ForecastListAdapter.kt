package com.bazyle.weatherservice.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bazyle.weatherservice.KELVIN_DIFF
import com.bazyle.weatherservice.R
import com.bazyle.weatherservice.model.ForecastItem
import com.bazyle.weatherservice.util.ImgLoader
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware
import kotlinx.android.synthetic.main.forecast_item_view.view.*
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import java.util.*

/**
 * Adapter for [RecyclerView] implementation, handling the data mapping to UI elements in the list.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */

class ForecastListAdapter : RecyclerView.Adapter<ForecastListAdapter.ViewHolder>() {
    private var forecastItems: LinkedList<ForecastItem>? = null

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val weatherIcon: ImageView = itemView.item_icon_weather
        val temperatureView: TextView = itemView.item_temp
        val weatherDescriptionView: TextView = itemView.item_weather_desc
        val dayOfWeekView: TextView = itemView.item_day_of_week
        val weatherHourView: TextView = itemView.item_hour
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.forecast_item_view, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position < 0 || position >= itemCount) return

        val (dt, _, weatherList, weatherIndicators) = forecastItems!![position]
        if (weatherList != null && weatherList.isNotEmpty()) {
            val (icon, description) = weatherList[0]
            ImgLoader.displayWeatherIcon(icon!!, ImageViewAware(holder.weatherIcon), null)
            holder.weatherDescriptionView.text = description
        }
        val itemTime = DateTime(dt * 1000, DateTimeZone.UTC)
        holder.dayOfWeekView.text = itemTime.dayOfWeek().asText //using default system locale
        holder.weatherHourView.text = itemTime.toString("H:mm")
        if (weatherIndicators != null) {
            holder.temperatureView.text = String.format(Locale.ENGLISH,
                    "%d °C", convertToCelsius(weatherIndicators.temperature))
        }
    }

    private fun convertToCelsius(kelvinDeg: Double) = Math.round(kelvinDeg - KELVIN_DIFF)

    override fun getItemCount() = forecastItems?.size ?: 0

    fun setForecastItems(forecastItems: List<ForecastItem>) {
        this.forecastItems = LinkedList(forecastItems)
    }

    fun reset() {
        forecastItems?.clear()
        //can be optimized - if needed
        notifyDataSetChanged()
    }
}