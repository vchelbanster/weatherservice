package com.bazyle.weatherservice.ui

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.os.Messenger
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import butterknife.ButterKnife
import butterknife.OnClick
import com.bazyle.weatherservice.App
import com.bazyle.weatherservice.R
import com.bazyle.weatherservice.service.JobUtil
import com.bazyle.weatherservice.service.WeatherJobService
import com.bazyle.weatherservice.util.isLocationPermissionGranted
import com.bazyle.weatherservice.util.showMessage
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

/**
 * [Activity][android.support.v4.app.FragmentActivity] acting as a high-level
 * container/controller for the visual content.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var jobUtil: JobUtil
    // Handler for incoming messages from the service.
    private var handler: IncomingMessageHandler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        App.component.inject(this)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        handler = IncomingMessageHandler(this)
    }

    override fun onStart() {
        super.onStart()
        val jobServiceIntent = Intent(this, WeatherJobService::class.java)
        val incoming = Messenger(handler)
        jobServiceIntent.putExtra(WeatherJobService.MESSENGER_INTENT_KEY, incoming)
        //start the service - providing a communication channel with the UI
        startService(jobServiceIntent)
    }

    override fun onStop() {
        //Whether if it's bound or not, the service was started explicitly so need to stop it the same way
        //otherwise - it'll hang around for a long time
        stopService(Intent(this, WeatherJobService::class.java))
        super.onStop()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        checkLocationPermission()
    }

    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), PERM_REQ)
    }


    private fun locationPermissionGranted() {
        jobUtil.scheduleJob()
    }

    @OnClick(R.id.fab)
    fun locationRequested() = checkLocationPermission()

    /**
     * Checks for location permission and informs the user about the actions.
     */
    private fun checkLocationPermission() {
        //Check if location permission has been granted
        if (this.isLocationPermissionGranted()) {
            locationPermissionGranted()
        } else {
            //has denied permission in the past?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                //show a snackbar explaining the request
                fab?.showMessage(R.string.location_permission_rationale) { this.requestLocationPermission() }
            } else {
                //request permission directly
                requestLocationPermission()
            }
        }
    }

    companion object {
        private val PERM_REQ = 12
    }
}