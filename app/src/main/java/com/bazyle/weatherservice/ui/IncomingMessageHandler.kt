package com.bazyle.weatherservice.ui

import android.os.Handler
import android.os.Message
import android.os.Messenger
import android.support.design.widget.FloatingActionButton

import com.bazyle.weatherservice.R
import com.bazyle.weatherservice.service.WeatherJobService
import com.bazyle.weatherservice.util.showPermanentMessage
import com.bazyle.weatherservice.util.showShortMessage

import java.lang.ref.WeakReference

/**
 * A [Handler] allowing to send messages associated with a thread. A [Messenger]
 * uses this handler to communicate from [WeatherJobService].
 */
internal class IncomingMessageHandler(activity: MainActivity) : Handler() {
    // Prevent possible leaks with a weak reference.
    private val activity: WeakReference<MainActivity> = WeakReference(activity)

    override fun handleMessage(msg: Message) {
        val mainActivity = activity.get()
                ?: // Activity is no longer available, drop.
                return
        val fab = mainActivity.findViewById<FloatingActionButton>(R.id.fab)
        when (msg.what) {
            /*
             * Receives callback from the service when JobScheduler
             * has called the service to execute its job.
             */
            WeatherJobService.MSG_UPDATE_STARTED -> {
                fab.showPermanentMessage(R.string.status_update_progress)
                fab.hide()
            }
            /*
             * Receives callback from the service when job execution has finished.
             */
            WeatherJobService.MSG_UPDATE_COMPLETED -> {
                fab.showShortMessage(R.string.status_update_success)
                fab.show()
            }
            /*
             * Receives callback from the service when job execution has finished.
             */
            WeatherJobService.MSG_UPDATE_COMPLETED_ERRORS -> {
                fab.showShortMessage(R.string.status_update_error)
                fab.show()
            }
        }
    }
}