package com.bazyle.weatherservice.net

import java.util.concurrent.TimeUnit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import com.bazyle.weatherservice.BASE_URL

/**
 * Convenience factory for [WeatherService] REST service.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
class RetrofitAdapter {

    fun buildApiService(): WeatherService {
        val builder = Retrofit.Builder()
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
                .addInterceptor(logInterceptor)
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()
        val retrofit = builder.baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
        return retrofit.create(WeatherService::class.java)
    }

}
