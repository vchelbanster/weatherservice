package com.bazyle.weatherservice.net

import com.bazyle.weatherservice.*
import com.bazyle.weatherservice.model.ForecastResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * {@link retrofit2.Retrofit}-specific contract for REST endpoints.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
interface WeatherService {
    /**
     * Retrieve the forecast for 5 days, with 3-hour timeframe using explicit location coordinates.
     *
     * @param latitude  latitude for the requested location
     * @param longitude longitude for the requested location
     * @return A [Retrofit wrapper][Call], which would return a wrapped [ForecastResponse] when executed, allowing for HTTP status check.
     */
    @GET(FORECAST_SUFFIX + APP_ID_PARAM)
    fun get5DayForecast(@Query(LAT_KEY) latitude: String, @Query(LON_KEY) longitude: String): Call<ForecastResponse>
}