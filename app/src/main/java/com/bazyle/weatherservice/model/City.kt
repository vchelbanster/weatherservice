package com.bazyle.weatherservice.model

import com.google.gson.annotations.SerializedName

data class City(
        @SerializedName("country")
        val country: String? = null,
        @SerializedName("coord")
        val coord: Coord? = null,
        @SerializedName("name")
        val name: String? = null,
        @SerializedName("id")
        val id: Int = 0
)