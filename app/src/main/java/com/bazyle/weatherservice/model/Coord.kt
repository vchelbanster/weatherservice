package com.bazyle.weatherservice.model

import com.google.gson.annotations.SerializedName

class Coord(@SerializedName("lon") val lon: Double = 0.toDouble(),
            @SerializedName("lat") val lat: Double = 0.toDouble())