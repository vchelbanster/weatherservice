package com.bazyle.weatherservice.model

import com.google.gson.annotations.SerializedName

data class WeatherIndicators(
        @SerializedName("temp")
        val temperature: Double = 0.toDouble(),
        @SerializedName("humidity")
        val humidity: Int = 0,
        @SerializedName("pressure")
        val pressure: Double = 0.toDouble()
)