package com.bazyle.weatherservice.model

import com.google.gson.annotations.SerializedName

data class ForecastItem(
        @SerializedName("dt")
        val dt: Long = 0,
        @SerializedName("dt_txt")
        val dtTxt: String? = null,
        @SerializedName("weather")
        val weather: List<WeatherItem>? = null,
        @SerializedName("main")
        val weatherIndicators: WeatherIndicators? = null
)