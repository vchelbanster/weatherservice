package com.bazyle.weatherservice.model

import com.google.gson.annotations.SerializedName

data class ForecastResponse(
        @SerializedName("city")
        val city: City? = null,
        @SerializedName("cnt")
        val cnt: Int = 0,
        @SerializedName("cod")
        val cod: String? = null,
        @SerializedName("message")
        val message: Double = 0.toDouble(),
        @SerializedName("list")
        val list: List<ForecastItem>? = null
)