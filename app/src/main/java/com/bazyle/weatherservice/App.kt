package com.bazyle.weatherservice

import android.app.Application

import com.bazyle.weatherservice.di.AppComponent
import com.bazyle.weatherservice.di.AppModule
import com.bazyle.weatherservice.di.DaggerAppComponent
import com.bazyle.weatherservice.util.ImgLoader


/**
 * @author Vasile Chelban
 * @since 20/08/2018
 */

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        ImgLoader.initImageLoader(this)
        component = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    companion object {
        @JvmStatic
        lateinit var component: AppComponent
    }
}
