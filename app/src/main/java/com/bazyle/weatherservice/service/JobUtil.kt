package com.bazyle.weatherservice.service

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context

import javax.inject.Inject

import android.content.Context.JOB_SCHEDULER_SERVICE
import com.bazyle.weatherservice.JOB_INTERVAL_MILLIS

/**
 * Utility for managing background work.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
class JobUtil @Inject constructor() {
    @Inject
    lateinit var ctx: Context

    private val isJobScheduled: Boolean
        get() {
            val scheduler = ctx.getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler

            val jobs = scheduler.allPendingJobs
            return jobs.size > 0 && jobs[0].id == WeatherJobService.JOB_ID
        }

    fun scheduleJob() {
        //schedule only if needed
        if (isJobScheduled) return

        val scheduler = ctx.getSystemService(JOB_SCHEDULER_SERVICE) as JobScheduler

        val component = ComponentName(ctx, WeatherJobService::class.java)
        val builder = JobInfo.Builder(WeatherJobService.JOB_ID, component)
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setPersisted(true)
                .setPeriodic(JOB_INTERVAL_MILLIS)

        //attempt to any exiting job prior scheduling a new one
        scheduler.cancelAll()
        scheduler.schedule(builder.build())
    }
}