package com.bazyle.weatherservice.service

import android.util.Log

import com.bazyle.weatherservice.model.ForecastResponse
import com.bazyle.weatherservice.net.WeatherService
import com.bazyle.weatherservice.util.FileHandler
import com.bazyle.weatherservice.util.SharedPrefs
import com.google.gson.Gson
import com.google.gson.JsonIOException

import java.io.IOException
import java.io.Writer

import javax.inject.Inject

import retrofit2.Response

/**
 * Data source abstraction allowing decoupling of data logic and android UI components.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
class RemoteDataSource @Inject constructor() {
    @Inject
    lateinit var weatherService: WeatherService
    @Inject
    lateinit var prefs: SharedPrefs
    @Inject
    lateinit var fileHandler: FileHandler
    @Inject
    lateinit var gson: Gson

    fun downloadForecastJson(): Boolean {
        val response: Response<ForecastResponse>
        try {
            response = weatherService.get5DayForecast(prefs.locationLat, prefs.locationLon).execute()
        } catch (e: IOException) {
            Log.e(TAG, "Network error occurred while retrieving forecast data", e)
            return false
        }

        if (response.isSuccessful && response.body() != null) {
            val forecastResponse = response.body()
            Log.v(TAG, String.format("Received data for Location (%s, %s) - %s",
                    prefs.locationLat, prefs.locationLon, forecastResponse!!.city))

            try {
                fileHandler.jsonDataWriter!!.use { writer ->
                    writer.write(gson.toJson(forecastResponse))
                    writer.flush()
                    return true
                }
            } catch (e: JsonIOException) {
                Log.e(TAG, "Failed to persist json data to file", e)
            } catch (e: IOException) {
                Log.e(TAG, "Failed to persist json data to file", e)
            }

        }
        return false
    }

    companion object {
        private val TAG = RemoteDataSource::class.java.simpleName
    }
}