package com.bazyle.weatherservice.service

import android.app.Service
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.os.Messenger
import android.os.RemoteException
import android.util.Log

import com.bazyle.weatherservice.App
import com.bazyle.weatherservice.BuildConfig

import java.util.concurrent.ExecutorService

import javax.inject.Inject

import com.bazyle.weatherservice.JOB_LOCATION_WAIT_TIMEOUT_MILLIS


/**
 * [JobService] handling data retrieval from [Rest service][com.bazyle.weatherservice.net.WeatherService]
 * and persisting the result.
 *
 *
 * This service is communicating with the [com.bazyle.weatherservice.ui.MainActivity] as well to provide in-time updates (WIP).
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
class WeatherJobService : JobService() {
    @Inject
    lateinit var dataSource: RemoteDataSource
    @Inject
    lateinit var executor: ExecutorService
    private var messenger: Messenger? = null
    private var mainHandler: Handler? = null

    override fun onCreate() {
        super.onCreate()
        App.component.inject(this)
        mainHandler = Handler(Looper.myLooper())
        Log.i(TAG, "WeatherService created")
    }

    override fun onDestroy() {
        executor.shutdownNow()
        super.onDestroy()
        Log.i(TAG, "WeatherService destroyed")
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        messenger = intent.getParcelableExtra(MESSENGER_INTENT_KEY)
        return Service.START_NOT_STICKY
    }

    override fun onStartJob(params: JobParameters): Boolean {
        Log.v(TAG, "Starting WeatherJobService execution")
        sendMessage(MSG_UPDATE_STARTED, null)

        //Starting location service - update to the latest known location
        LocationService.startActionLocate(this)

        executor.execute { retrieveLatestForecast(params) }
        return true
    }

    private fun retrieveLatestForecast(params: JobParameters?) {
        //Wait a bit update location - LocationService
        try {
            Thread.sleep(JOB_LOCATION_WAIT_TIMEOUT_MILLIS)
        } catch (e: InterruptedException) {
            Log.w(TAG, "Can't buildApiService a good sleep maan?")
        }

        val needMoreWork = !dataSource.downloadForecastJson()
        mainHandler!!.post {
            sendMessage(if (needMoreWork) MSG_UPDATE_COMPLETED_ERRORS else MSG_UPDATE_COMPLETED, null)
            if (params != null) {
                jobFinished(params, needMoreWork)
            }
        }
    }

    override fun onStopJob(params: JobParameters): Boolean {
        //todo: add differentiated message for forced finish
        sendMessage(MSG_UPDATE_COMPLETED, null)
        executor.shutdown()
        return false
    }

    private fun sendMessage(messageID: Int, params: Any?) {
        // If this service is launched by the JobScheduler, there's no callback Messenger. It
        // only exists when the MainActivity calls startService() with the callback in the Intent.
        if (messenger == null) {
            Log.d(TAG, "Service is bound, not started. There's no callback to send a message to.")
            return
        }
        val m = Message.obtain()
        m.what = messageID
        m.obj = params
        try {
            messenger!!.send(m)
        } catch (e: RemoteException) {
            Log.e(TAG, "Error passing service object back to activity.")
        }

    }

    companion object {

        val JOB_ID = 23
        val MESSENGER_INTENT_KEY = BuildConfig.APPLICATION_ID + ".MESSENGER_INTENT_KEY"
        val MSG_UPDATE_STARTED = 0
        val MSG_UPDATE_COMPLETED = 1
        val MSG_UPDATE_COMPLETED_ERRORS = 2
        private val TAG = WeatherJobService::class.java.simpleName
    }

}