package com.bazyle.weatherservice.service

import android.annotation.SuppressLint
import android.app.IntentService
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.location.Location
import android.text.TextUtils
import android.util.Log

import com.bazyle.weatherservice.App
import com.bazyle.weatherservice.R
import com.bazyle.weatherservice.util.SharedPrefs
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult

import java.util.ArrayList
import java.util.concurrent.TimeUnit

import javax.inject.Inject

import com.bazyle.weatherservice.GOOGLE_API_CLIENT_TIMEOUT
import com.bazyle.weatherservice.LOCATION_ACCURACY_RADIUS
import com.bazyle.weatherservice.LOCATION_UPDATES_MAX_DURATION
import com.bazyle.weatherservice.LOCATION_UPDATES_WAIT_TIME
import com.bazyle.weatherservice.util.isLocationPermissionGranted
import com.google.android.gms.location.LocationServices.FusedLocationApi

/**
 * An [IntentService] handling location requests and updates on a separate handler thread.
 *
 *
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
class LocationService : IntentService("LocationService") {
    @Inject
    lateinit var sharedPrefs: SharedPrefs
    @Inject
    lateinit var apiClient: GoogleApiClient

    private var currentListenerIntent: PendingIntent? = null

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null && !TextUtils.isEmpty(intent.action)) {
            when (intent.action) {
                ACTION_LOCATE -> {
                    handleLocationRequest()
                }
                ACTION_LOCATION_UPDATE -> {
                    if (!LocationResult.hasResult(intent)) {
                        Log.v(TAG, "Received location update without a result - dropping...")
                        return
                    }
                    handleLocationUpdate(LocationResult.extractResult(intent).lastLocation)
                }
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        App.component.inject(this)
    }

    private fun handleLocationUpdate(location: Location) {
        Log.v(TAG, "Received location update: " + location.toString())
        sharedPrefs.saveLocation(location)

        if (location.accuracy < LOCATION_ACCURACY_RADIUS) {
            Log.v(TAG, "Received accurate location update: " + location.accuracy + ". Stopping location service.]")
            unsubscribeFromLocationUpdates()
            stopSelf()
        }
    }

    override fun onDestroy() {
        unsubscribeFromLocationUpdates()
        super.onDestroy()
    }

    /**
     * Handling location update requests.
     */
    @SuppressLint("MissingPermission")
    private fun handleLocationRequest() {
        Log.v(TAG, ACTION_LOCATE)
        if (!this.isLocationPermissionGranted()) {
            return
        }

        val result = apiClient.blockingConnect(GOOGLE_API_CLIENT_TIMEOUT.toLong(), TimeUnit.SECONDS)
        if (!result.isSuccess || !apiClient.isConnected) {
            Log.e(TAG, getString(R.string.google_api_error_msg, result.errorCode))
            return
        }

        unsubscribeFromLocationUpdates()

        val locationUpdateIntent = Intent(this, LocationService::class.java)
        locationUpdateIntent.action = ACTION_LOCATION_UPDATE

        // Send last known location out first if available
        val location = FusedLocationApi.getLastLocation(apiClient)
        if (location != null) {
            Log.v(TAG, "Location:\n" + location.toString())
            handleLocationUpdate(location)
            val lastLocationIntent = Intent(locationUpdateIntent)
            val locations = ArrayList<Location>()
            locations.add(location)
            lastLocationIntent.putExtra(KEY_LOCATION_CHANGED, LocationResult.create(locations))
            startService(lastLocationIntent)
        }

        // Request new location
        val locationRequest = LocationRequest()
                .setPriority(LocationRequest.PRIORITY_LOW_POWER)
                .setMaxWaitTime(LOCATION_UPDATES_WAIT_TIME)
                .setExpirationDuration(LOCATION_UPDATES_MAX_DURATION)
        FusedLocationApi.requestLocationUpdates(
                apiClient, locationRequest,
                PendingIntent.getService(this, 0, locationUpdateIntent, 0))
        apiClient.disconnect()
    }

    private fun unsubscribeFromLocationUpdates() {
        if (currentListenerIntent != null) {
            FusedLocationApi.removeLocationUpdates(apiClient, currentListenerIntent)
            currentListenerIntent = null
        }
    }

    companion object {
        private val TAG = LocationService::class.java.simpleName

        private val ACTION_LOCATE = "com.bazyle.weatherservice.service.action.LOCATE"
        private val ACTION_LOCATION_UPDATE = "com.bazyle.weatherservice.service.action.LOCATION_UPDATE"
        private val KEY_LOCATION_CHANGED = "com.google.android.gms.location.EXTRA_LOCATION_RESULT"

        /**
         * Starts the service to retrieve device's location. If
         * the service is already performing another task this action will be queued.
         *
         * @see IntentService
         */
        fun startActionLocate(context: Context) {
            val intent = Intent(context, LocationService::class.java)
            intent.action = ACTION_LOCATE
            context.startService(intent)
        }
    }
}