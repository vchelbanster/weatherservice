/**
 * App-specific constants
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
package com.bazyle.weatherservice

const val ICON_URL_MASK = "http://openweathermap.org/img/w/%s.png"
const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
const val FORECAST_SUFFIX = "forecast"

const val QUERY_KEY = "q"
const val LAT_KEY = "lat"
const val LON_KEY = "lon"

const val APP_ID_PARAM = "?APPID=5ad7218f2e11df834b0eaf3a33a39d2a"

/**
 * Connection timeout in seconds
 */
const val GOOGLE_API_CLIENT_TIMEOUT = 10

/**
 * 3 Hours
 */
const val JOB_INTERVAL_MILLIS = (3 * 60 * 60 * 1000).toLong()

/**
 * 2 Seconds waiting for fused location
 */
const val JOB_LOCATION_WAIT_TIMEOUT_MILLIS: Long = 2000

const val KELVIN_DIFF = 273.15f

//City/town precision - in meters
const val LOCATION_ACCURACY_RADIUS = 5000f
const val LOCATION_UPDATES_MAX_DURATION = 60000L
const val LOCATION_UPDATES_WAIT_TIME = 10000L
