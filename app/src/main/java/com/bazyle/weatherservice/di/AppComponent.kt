package com.bazyle.weatherservice.di

import com.bazyle.weatherservice.service.LocationService
import com.bazyle.weatherservice.service.WeatherJobService
import com.bazyle.weatherservice.ui.ForecastListFragment
import com.bazyle.weatherservice.ui.MainActivity

import javax.inject.Singleton

import dagger.Component

/**
 * App-level component serving as an initialization entry-point for Dagger.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)

    fun inject(fragment: ForecastListFragment)

    fun inject(service: WeatherJobService)

    fun inject(locationService: LocationService)
}