package com.bazyle.weatherservice.di

import android.content.Context

import com.bazyle.weatherservice.App
import com.bazyle.weatherservice.net.RetrofitAdapter
import com.bazyle.weatherservice.net.WeatherService
import com.bazyle.weatherservice.util.SharedPrefs
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

import javax.inject.Singleton

import dagger.Module
import dagger.Provides


/**
 * Dagger module that define custom/non-implicit bindings.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */

@Module
class AppModule(private val application: App) {


    @Provides
    @Singleton
    fun providesContext(): Context = application

    @Provides
    fun providesExecutor(): ExecutorService = Executors.newSingleThreadExecutor()

    @Provides
    @Singleton
    fun providesSharedPrefs(): SharedPrefs = SharedPrefs(application)

    @Provides
    @Singleton
    fun providesApiService(): WeatherService = RetrofitAdapter().buildApiService()

    @Provides
    fun providesGson(): Gson = Gson()

    @Provides
    fun providesGApiClient(): GoogleApiClient {
        return GoogleApiClient.Builder(application)
                .addApi(LocationServices.API)
                .build()
    }

}