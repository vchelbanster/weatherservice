package com.bazyle.weatherservice

import okhttp3.MediaType
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.ResponseBody


/**
 * Utility methods used for testing purposes.
 *
 * @author Vasile Chelban
 * @since 20/08/2018
 */

fun prepareHttpResponse(httpStatusCode: Int, message: String): okhttp3.Response {
    return okhttp3.Response.Builder()
            .request(Request.Builder().url("http://localhost/").build())
            .body(ResponseBody.create(MediaType.parse("application/json"), ""))
            .protocol(Protocol.HTTP_1_1)
            .code(httpStatusCode)
            .message(message)
            .build()
}
