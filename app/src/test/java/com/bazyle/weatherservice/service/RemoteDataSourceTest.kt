@file:Suppress("UNCHECKED_CAST")

package com.bazyle.weatherservice.service

import com.bazyle.weatherservice.model.ForecastResponse
import com.bazyle.weatherservice.net.WeatherService
import com.bazyle.weatherservice.util.FileHandler
import com.bazyle.weatherservice.util.SharedPrefs
import com.google.gson.Gson
import com.google.gson.JsonIOException

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate

import java.io.IOException
import java.io.Writer

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response

import com.bazyle.weatherservice.prepareHttpResponse
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyString
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito.atMost
import org.mockito.Mockito.doThrow
import org.mockito.Mockito.mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyZeroInteractions
import org.mockito.Mockito.`when`

@RunWith(PowerMockRunner::class)
@PowerMockRunnerDelegate(MockitoJUnitRunner::class)
@PrepareForTest(Gson::class)
class RemoteDataSourceTest {

    @Mock
    private val weatherService: WeatherService? = null
    @Mock
    private val prefs: SharedPrefs? = null
    @Mock
    private val fileHandler: FileHandler? = null
    @Mock
    private val gson: Gson? = null
    @Mock
    private val writer: Writer? = null
    @InjectMocks
    private var dataSource: RemoteDataSource? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @After
    fun tearDown() {
        dataSource = null
    }

    @Test
    @Throws(IOException::class)
    fun test_downloadForecastJson_success() {
        val dataResponse = Mockito.mock(ForecastResponse::class.java)
        val requestCallable = mock(Call::class.java) as Call<ForecastResponse>
        `when`(prefs!!.locationLat).thenReturn(DEFAULT_LATITUDE)
        `when`(prefs.locationLon).thenReturn(DEFAULT_LONGITUDE)
        `when`(weatherService!!.get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))).thenReturn(requestCallable)
        val wrappedResponse = Response.success(dataResponse, prepareHttpResponse(200, "OK"))
        `when`(requestCallable.execute()).thenReturn(wrappedResponse)

        `when`<Writer>(fileHandler!!.jsonDataWriter).thenReturn(writer)
        `when`(gson!!.toJson(any(ForecastResponse::class.java))).thenReturn("Some json")

        assertTrue(dataSource!!.downloadForecastJson())

        verify(weatherService).get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))
        verify<Writer>(writer).write(eq("Some json"))
        verify<Writer>(writer, atMost(1)).flush()
    }

    @Test
    @Throws(IOException::class)
    fun test_downloadForecastJson_failure_invalidJson() {
        val dataResponse = Mockito.mock(ForecastResponse::class.java)
        val requestCallable = mock(Call::class.java) as Call<ForecastResponse>
        `when`(prefs!!.locationLat).thenReturn(DEFAULT_LATITUDE)
        `when`(prefs.locationLon).thenReturn(DEFAULT_LONGITUDE)
        `when`(weatherService!!.get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))).thenReturn(requestCallable)
        val wrappedResponse = Response.success(dataResponse, prepareHttpResponse(200, "OK"))
        `when`(requestCallable.execute()).thenReturn(wrappedResponse)

        `when`<Writer>(fileHandler!!.jsonDataWriter).thenReturn(writer)
        `when`(gson!!.toJson(any(ForecastResponse::class.java))).thenThrow(JsonIOException("Invalid json"))

        assertFalse(dataSource!!.downloadForecastJson())

        verify(weatherService).get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))
        verify<Writer>(writer, times(0)).write(anyString())
        verify<Writer>(writer, times(0)).flush()
    }

    @Test
    @Throws(IOException::class)
    fun test_downloadForecastJson_failure_ioError() {
        val dataResponse = Mockito.mock(ForecastResponse::class.java)
        val requestCallable = mock(Call::class.java) as Call<ForecastResponse>
        `when`(prefs!!.locationLat).thenReturn(DEFAULT_LATITUDE)
        `when`(prefs.locationLon).thenReturn(DEFAULT_LONGITUDE)
        `when`(weatherService!!.get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))).thenReturn(requestCallable)
        val wrappedResponse = Response.success(dataResponse, prepareHttpResponse(200, "OK"))
        `when`(requestCallable.execute()).thenReturn(wrappedResponse)

        `when`<Writer>(fileHandler!!.jsonDataWriter).thenReturn(writer)
        `when`(gson!!.toJson(any(ForecastResponse::class.java))).thenReturn("Some json")
        doThrow(IOException("Not enough space on disk")).`when`<Writer>(writer).write(eq("Some json"))

        assertFalse(dataSource!!.downloadForecastJson())

        verify(weatherService).get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))
        verify<Writer>(writer).write(anyString())
        verify<Writer>(writer, times(0)).flush()
    }

    @Test
    @Throws(IOException::class)
    fun test_downloadForecastJson_failure_NoWriterAvailable() {
        val dataResponse = ForecastResponse()
        val requestCallable = mock(Call::class.java) as Call<ForecastResponse>
        `when`(prefs!!.locationLat).thenReturn(DEFAULT_LATITUDE)
        `when`(prefs.locationLon).thenReturn(DEFAULT_LONGITUDE)
        `when`(weatherService!!.get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))).thenReturn(requestCallable)
        val wrappedResponse = Response.success(dataResponse, prepareHttpResponse(200, "OK"))
        `when`(requestCallable.execute()).thenReturn(wrappedResponse)
        `when`<Writer>(fileHandler!!.jsonDataWriter).thenReturn(null)

        assertFalse(dataSource!!.downloadForecastJson())

        verify(weatherService).get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))
        verifyZeroInteractions(writer)
    }


    @Test
    @Throws(IOException::class)
    fun test_downloadForecastJson_failure_invalidHttpBody() {
        val requestCallable = mock(Call::class.java) as Call<ForecastResponse>
        `when`(prefs!!.locationLat).thenReturn(DEFAULT_LATITUDE)
        `when`(prefs.locationLon).thenReturn(DEFAULT_LONGITUDE)
        `when`(weatherService!!.get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))).thenReturn(requestCallable)
        val wrappedResponse = Response.success<ForecastResponse>(null, prepareHttpResponse(200, "OK"))
        `when`(requestCallable.execute()).thenReturn(wrappedResponse)

        assertFalse(dataSource!!.downloadForecastJson())

        verify(weatherService).get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))
        verifyZeroInteractions(fileHandler)
        verifyZeroInteractions(writer)
    }

    @Test
    @Throws(IOException::class)
    fun test_downloadForecastJson_failure_serverError() {
        val requestCallable = mock(Call::class.java) as Call<ForecastResponse>
        `when`(prefs!!.locationLat).thenReturn(DEFAULT_LATITUDE)
        `when`(prefs.locationLon).thenReturn(DEFAULT_LONGITUDE)
        `when`(weatherService!!.get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))).thenReturn(requestCallable)
        val responseBody = mock(ResponseBody::class.java)
        val wrappedResponse = Response.error<ForecastResponse>(responseBody, prepareHttpResponse(400, "Not allowed"))
        `when`(requestCallable.execute()).thenReturn(wrappedResponse)

        assertFalse(dataSource!!.downloadForecastJson())

        verify(weatherService).get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))
        verifyZeroInteractions(fileHandler)
        verifyZeroInteractions(writer)
    }

    @Test
    @Throws(IOException::class)
    fun test_downloadForecastJson_failure_networkError() {
        val requestCallable = mock(Call::class.java) as Call<ForecastResponse>
        `when`(prefs!!.locationLat).thenReturn(DEFAULT_LATITUDE)
        `when`(prefs.locationLon).thenReturn(DEFAULT_LONGITUDE)
        `when`(weatherService!!.get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))).thenReturn(requestCallable)
        `when`(requestCallable.execute()).thenThrow(IOException("Connection timeout"))

        assertFalse(dataSource!!.downloadForecastJson())

        verify(weatherService).get5DayForecast(eq(DEFAULT_LATITUDE), eq(DEFAULT_LONGITUDE))
        verifyZeroInteractions(fileHandler)
        verifyZeroInteractions(writer)
    }

    companion object {
        private val DEFAULT_LATITUDE = "latitude"
        private val DEFAULT_LONGITUDE = "longitude"
    }
}